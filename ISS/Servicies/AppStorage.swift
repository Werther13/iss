//
//  AppStorage.swift
//  ISS
//
//  Created by Lobont Andrei on 12/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

private struct Keys {
    static let isFreshInstallKey: String = "isFreshInstall"
    static let ridesHistoryKey: String = "ridesHistory"
}

public class AppStorage {
    static var isFreshInstall: Bool  {
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.isFreshInstallKey)
        }
        get {
            if let value = UserDefaults.standard.object(forKey: Keys.isFreshInstallKey) as? Bool {
                return value
            }
            
            return true
        }
    }
    
    static var ridesHistory: [Ride]  {
        get {
            if UserDefaults.standard.object(forKey: Keys.ridesHistoryKey) != nil {
                if let data = UserDefaults.standard.value(forKey: Keys.ridesHistoryKey) as? Data {
                    let myObject = try? PropertyListDecoder().decode([Ride].self, from: data)
                    return myObject!
                }
            }
            return []
        }
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: Keys.ridesHistoryKey)
        }
    }
}
