//
//  URLSession+Extensions.swift
//  ISS
//
//  Created by Lobont Andrei on 15/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

extension URL {
    static let base = URL(string: "http://188.24.33.93:3286")!
}

extension URLSession {
    func requestAuth<T: Decodable>(url: URL, method: HTTPMethod, parameters: [String : Any], completion: @escaping (AuthenticationResponse<T, RequestError>) -> Void) {
        
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            try urlRequest.encode(parameters: parameters, for: method)
        } catch {
            print("Encountered error while encoding parameters for request")
            var requestError = RequestError.badRequest
            if let error = error as? RequestError {
                requestError = error
            }
            completion(.failure(requestError))
            return
        }
        
        urlRequest.httpMethod = method.rawValue
        
        self.dataTask(with: urlRequest) { (data, response, error) in
            
            guard error == nil else {
                print("An error has occurred \(error?.localizedDescription ?? "error does not have a description")")
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                print("ceva response prost")
                return
            }
            
            if httpResponse.statusCode == 200, let data = data {
                do {
                    let accessToken = httpResponse.allHeaderFields["Access-Client"] as? String
                    let dataResponse = try JSONDecoder().decode(T.self, from: data)
                    DispatchQueue.main.async {
                        completion(.success(dataResponse, accessToken))
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(.failure(.deserializationFailure))
                    }
                }
                return
            }
            DispatchQueue.main.async {
                completion(.failure(.badRequest))
            }
        }.resume()
    }
    
    func requestSignOut(url: URL, method: HTTPMethod, headerParameters: [String : String], completion: @escaping (() -> Void)) {
        
        var urlRequest = URLRequest(url: url)
        urlRequest.allHTTPHeaderFields = headerParameters
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        urlRequest.httpMethod = method.rawValue
        
        self.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                completion()
            }
        }.resume()
    }
    
    func request<T: Decodable>(url: URL, method: HTTPMethod, parameters: [String: Any], completion: @escaping (Result<T, RequestError>) -> ()) {
        
        var request = URLRequest(url: url)
        
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json"
        ]
        
        do {
            try request.encode(parameters: parameters, for: method)
        } catch let requestError as RequestError {
            completion(.failure(requestError))
            return
        } catch {
            
        }
        
        request.httpMethod = method.rawValue
        
        self.dataTask(with: request) { (data: Data?, response: URLResponse?, responseError: Error?) in
            guard let httpResponse = response as? HTTPURLResponse else {
                return
            }
            
            if httpResponse.statusCode == 200 {
                do {
                    let data1 = try JSONDecoder().decode(T.self, from: data!)
                    DispatchQueue.main.async {
                        completion(.success(data1))
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(.failure(.deserializationFailure))
                    }
                    
                }
                return
            }
            DispatchQueue.main.async {
                completion(.failure(.badRequest))
            }
            }.resume()
    }

}


fileprivate extension URLRequest {
    mutating func jsonEncode(parameters: [String: Any]) throws {
        guard let body = try? JSONSerialization.data(withJSONObject: parameters) else {
            throw RequestError.deserializationFailure
        }
        httpBody = body
    }
    
    mutating func urlEncode(parameters: [String: Any]) {
        let queryItems = parameters.map { (item) -> URLQueryItem in
            URLQueryItem(name: item.key, value: "\(item.value)")
        }
        
        var components = URLComponents(url: self.url!, resolvingAgainstBaseURL: true)
        components?.queryItems = queryItems
        self.url = components?.url
    }
    
    mutating func encode(parameters: [String: Any], for method: HTTPMethod) throws {
        switch method {
        case .get:
            urlEncode(parameters: parameters)
        case .post, .delete:
            try jsonEncode(parameters: parameters)
        }
    }
    
}
