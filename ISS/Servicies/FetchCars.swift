//
//  FetchCars.swift
//  ISS
//
//  Created by Lobont Andrei on 15/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

private extension String {
    static let carsEndPoint = "/api/cars"
}

protocol FetchCarsProtocol {
    func fetchCars(completion: @escaping (Result<[Car], RequestError>) -> ())
}

class FetchCars: FetchCarsProtocol {
    private let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func fetchCars(completion: @escaping (Result<[Car], RequestError>) -> ()) {
        let url = URL.base.appendingPathComponent(String.carsEndPoint)
        session.request(url: url, method: .get, parameters: [:]) { [weak self] (result: Result<[Car], RequestError>) in
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let cars):
                completion(.success(cars))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
