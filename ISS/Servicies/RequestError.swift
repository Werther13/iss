//
//  RequestError.swift
//  ISS
//
//  Created by Lobont Andrei on 15/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

enum RequestError: Error {
    case badRequest
    case deserializationFailure
    
    var localizedDescription: String {
        switch self {
        case .badRequest:
            return "Response was 400."
        case .deserializationFailure:
            return "Failure to desearialize data."
        }
    }
}
