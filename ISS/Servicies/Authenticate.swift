//
//  Authenticate.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

private extension String {
    static let loginEndPoint = "/api/login"
}

enum AuthenticationResponse<Result, Error> {
    case success (Result, String?)
    case failure (Error)
}

protocol AuthenticationServiceProtocol {
    var user: User? { get }
    func authenticate(email: String, password: String, completion: @escaping (Result<User, RequestError>) -> ())
    func signOut()
    func isAuthenticated() -> Bool
}

class AuthenticationService: AuthenticationServiceProtocol {
    private(set) var user: User? {
        get {
            guard let persistentValue = UserDefaults.standard.value(forKey: "user") as? Data,
                let user = try? JSONDecoder().decode(User.self, from: persistentValue) else {
                    return nil
            }
            return user
        }
        
        set {
            let data = try? JSONEncoder().encode(newValue)
            UserDefaults.standard.set(data, forKey: "user")
        }
    }
    
    private(set) var accessToken: String? {
        get {
            return UserDefaults.standard.value(forKey: "accessToken") as? String
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "accessToken")
        }
    }

    
    private let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func authenticate(email: String, password: String, completion: @escaping (Result<User, RequestError>) -> ())  {
        
        let url = URL.base.appendingPathComponent(String.loginEndPoint)
        session.requestAuth(url: url, method: .get, parameters: ["Mail": email, "Password": password]) {
           [weak self] (result: AuthenticationResponse<User, RequestError>) in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let user, let accessToken):
                strongSelf.user = user
                strongSelf.accessToken = accessToken
                completion(.success(user))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func isAuthenticated() -> Bool {
        return user != nil
    }
    
    func signOut() {
        user = nil
    }
}
