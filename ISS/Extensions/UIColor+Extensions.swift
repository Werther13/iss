//
//  UIColor+Extensions.swift
//  ISS
//
//  Created by Lobont Andrei on 05/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let issGreen = UIColor(red: 76, green: 175, blue: 80)
    
    private convenience init(red: CGFloat, green: CGFloat, blue: CGFloat) {
        self.init(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: 1.0)
    }
}
