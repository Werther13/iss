//
//  UITextField+Extensions.swift
//  ISS
//
//  Created by Lobont Andrei on 05/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation
import UIKit

class UnderlinedTextField: UITextField {
    private static let borderLineHeight: CGFloat = 0.5
    private static let selectedborderLineHeight: CGFloat = 2.0
    private let border = CALayer()

    private var lineHeight : CGFloat {
        didSet {
            border.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)

            border.borderWidth = lineHeight
        }
    }

    required init?(coder aDecoder: NSCoder) {
        lineHeight = UnderlinedTextField.borderLineHeight
        super.init(coder: aDecoder)

        addBorder()
    }

    override func draw(_ rect: CGRect) {
        border.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.addTarget(self, action: #selector(textFieldDidEndEditing(_:)), for: .editingDidEnd)
        self.addTarget(self, action: #selector(textFieldDidBeginEditing(_:)), for: .editingDidBegin)
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return textPaddedRect(forBounds: bounds)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textPaddedRect(forBounds: bounds)
    }

    @objc private func textFieldDidBeginEditing(_ textField: UITextField) {
        border.borderColor = UIColor.issGreen.cgColor
        lineHeight = UnderlinedTextField.selectedborderLineHeight
    }

    @objc private func textFieldDidEndEditing(_ textField: UITextField) {
        border.borderColor = UIColor.issGreen.cgColor
        lineHeight = UnderlinedTextField.borderLineHeight
    }

    private func addBorder() {
        border.borderColor = UIColor.gray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = lineHeight
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

    private func textPaddedRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.width, height: bounds.height - 10.0)
    }
}

