//
//  UIViewController+Extensions.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardOnScreenTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOnView(sender:)))

        view.addGestureRecognizer(tapGesture)
    }

    @objc private func didTapOnView(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}
