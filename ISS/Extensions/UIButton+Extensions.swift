//
//  UIButton+Extensions.swift
//  ISS
//
//  Created by Lobont Andrei on 05/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class GreenButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        setupStyle()
        setupLabel()
    }
    
    private func setupStyle() {
        backgroundColor = UIColor.issGreen
        roundCorners()
    }
    
    private func setupLabel() {
        setTitleColor(UIColor.black, for: .normal)
    }
    
    private func roundCorners() {
        layer.cornerRadius = 16.0
        layer.masksToBounds = true
    }
}
