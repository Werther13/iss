//
//  UIView+Extensions.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

extension UIView {
    func show() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.transform = .identity
        }, completion: nil)
    }
    
    func hide() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
        }, completion: nil)
    }
}
