//
//  CarItem.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

enum CarItem: CaseIterable {
    case engine
    case notOpening
    case windshield
    case lights
    case other
    
    var title: String {
        
        switch self {
        case .engine:
            return "Report and engine problem"
        case .notOpening:
            return "Report a problem with the doors"
        case .windshield:
            return "Report a windshield or windows problem"
        case .lights:
            return "Report a problem with a light"
        case .other:
            return "Other"
        }
    }
}
