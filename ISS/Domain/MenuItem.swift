//
//  MenuItem.swift
//  ISS
//
//  Created by Andrei Lobont on 11/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

enum MenuItem: CaseIterable {
    case feedback
    case vouchers
    case about
    case terms
    case reportBug
    case reportCar
    case signOut
    
    var title: String {
        
        switch self {
        case .feedback:
            return "Feedback"
        case .vouchers:
            return "Vouchers"
        case .about:
            return "About"
        case .terms:
            return "Terms and Conditions"
        case .reportBug:
            return "Report a Bug"
        case .reportCar:
            return "Report a Car Problem"
        case .signOut:
            return "Sign Out"
        }
    }
    
    var imageName: String {
        
        switch self {
        case .feedback:
            return "feedback"
        case .vouchers:
            return "voucherMenu"
        case .about:
            return "about"
        case .terms:
            return "terms"
        case .reportBug:
            return "reportBug"
        case .reportCar:
            return "reportCar"
        case .signOut:
            return "signOut"
        }
    }
}
