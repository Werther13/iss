//
//  BugItem.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

enum BugItem: CaseIterable {
    case map
    case profile
    case feedback
    case payment
    case other
    
    var title: String {
        
        switch self {
        case .map:
            return "Report a map bug"
        case .profile:
            return "Report a profile bug"
        case .feedback:
            return "Repot a feedback bug"
        case .payment:
            return "Report a payment bug"
        case .other:
            return "Other bugs"
        }
    }
}
