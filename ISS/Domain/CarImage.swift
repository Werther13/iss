//
//  CarImage.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

enum CarImage: String {
    case peugeot
    case audi
    case volkswagen
    case bmw
    case ford
    case opel
    case other
    
    var name: String {
        
        switch self {
        case .peugeot:
            return "peugeot"
        case .audi:
            return "audi"
        case .volkswagen:
            return "volkswagen"
        case .bmw:
            return "bmw"
        case .ford:
            return "ford"
        case .opel:
            return "opel"
        case .other:
            return "noImage"
        }
    }
}
