//
//  Car.swift
//  ISS
//
//  Created by Lobont Andrei on 15/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

struct Car: Decodable {
    let manufacturer: String
    let model: String
    let numberOfSeats: Int
    let price: Int
    let state: Int
    let id: Int
    let coordinates: Coordinates
    
    private enum CodingKeys: String, CodingKey {
        case manufacturer = "manufacturer"
        case model = "model"
        case numberOfSeats = "numberOfSeats"
        case price = "price"
        case state = "state"
        case id = "id"
        case coordinates = "coordinates"
    }
}
