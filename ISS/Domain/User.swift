//
//  User.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

struct User: Codable {
    let name: String
    let mail: String
    let password: String
    let numberOfPastRentals: Int
    
    private enum CodingKeys: String, CodingKey {
        case name = "Name"
        case mail = "Mail"
        case password = "Password"
        case numberOfPastRentals = "NumberOfPastRentals"
    }
}
