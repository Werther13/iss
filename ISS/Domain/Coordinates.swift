//
//  Coordinates.swift
//  ISS
//
//  Created by Lobont Andrei on 22/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

struct Coordinates: Decodable {
    let x: Double
    let y: Double
}
