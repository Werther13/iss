//
//  RIde.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

struct Ride: Codable {
    let adress: String
    let date: String
    let car: String
}
