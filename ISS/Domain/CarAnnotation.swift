//
//  CarAnnotation.swift
//  ISS
//
//  Created by Lobont Andrei on 22/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation
import MapKit

class CarAnnotation: NSObject, MKAnnotation {
    let car: Car
    var coordinate: CLLocationCoordinate2D
    
    init(car: Car) {
        self.car = car
        self.coordinate = CLLocationCoordinate2D(latitude: car.coordinates.x, longitude: car.coordinates.y)
    }
    
    func getCarForAnnotation() -> Car {
        return self.car
    }
}
