//
//  MenuPresenter.swift
//  ISS
//
//  Created by Andrei Lobont on 11/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class MenuPresenter {
    private let menuItems = [MenuItem.about, MenuItem.feedback, MenuItem.reportBug, MenuItem.reportCar, MenuItem.vouchers, MenuItem.terms, MenuItem.signOut]
    private weak var view: MenuView?
    private let authenticateService: AuthenticationServiceProtocol = AuthenticationService()
    
    init(view: MenuView) {
        self.view = view
    }
    
    func numberOfRows() -> Int {
        return menuItems.count
    }
    
    func configure(_ cell: MenuTableViewCellView, at indexPath: IndexPath) {
        let item = menuItems[indexPath.row]
        
        cell.set(name: item.title)
        cell.set(image: item.imageName)
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        let item = menuItems[indexPath.row]
        navigateToMenu(item)
    }
    
    private func navigateToMenu(_ item: MenuItem) {
        switch item {
        case .feedback:
            view?.navigateToFeedback()
        case .vouchers:
            view?.navigateToVouchers()
        case .about:
            view?.navigateToAbout()
        case .terms:
            view?.navigateToTerms()
        case .reportBug:
            view?.navigateToReportBug()
        case .reportCar:
            view?.navigateToReportCar()
        case .signOut:
            authenticateService.signOut()
            view?.signOut()
        }
    }
}
