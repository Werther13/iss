//
//  ReportBugPresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class ReportBugPresenter {
    private weak var view: ReportBugView?
    private var lastItemView: ReportBugCellView?
    private var selectedIndex: Int = 0
    private let bugItems: [BugItem] = [BugItem.feedback, BugItem.map, BugItem.payment, BugItem.profile, BugItem.other]
    
    init(view: ReportBugView) {
        self.view = view
    }
    
    func continuePressed() {
        view?.navigateToBugDetails(with: bugItems[selectedIndex])
    }
    
    func numberOfRows() -> Int {
        return bugItems.count
    }
    
    func configure(_ itemView: ReportBugCellView, at index: Int) {
        let bug = bugItems[index]
        itemView.set(content: bug.title)
    }
    
    func didSelect(_ itemView: ReportBugCellView, at index: Int) {
        view?.enableContinue()
        if lastItemView != nil {
            lastItemView?.setDeselected()
        }
        itemView.setSelected()
        lastItemView = itemView
    }
}
