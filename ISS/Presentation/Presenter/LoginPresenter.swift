//
//  LoginPresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 12/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class LoginPresenter {
    private weak var view: LoginView?
    private weak var navigation: LoginNavigation?
    private let authenticateService: AuthenticationServiceProtocol = AuthenticationService()
    
    init(view: LoginView, navigation: LoginNavigation) {
        self.view = view
        self.navigation = navigation
    }
    
    func login(with email: String, and password: String) {
        authenticateService.authenticate(email: email, password: password) { (result: Result<User, RequestError>) in
            switch result {
            case .success(_):
                self.navigation?.navigateToHome()
            case .failure(let error):
                self.view?.showLoginFailedAlert()
            }
        }
    }
}
