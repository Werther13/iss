//
//  RidesHistoryPresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class RidesHistoryPresenter {
    private weak var view: RidesHistoryView?
    var rides: [Ride] = AppStorage.ridesHistory
    
    init(view: RidesHistoryView) {
        self.view = view
    }
    
    func numberOfRows() -> Int {
        return rides.count
    }
    
    func configure(_ itemView: RidesHistoryCellView, at index: Int) {
        let ride = rides[index]
        
        itemView.set(adress: ride.adress)
        itemView.set(date: ride.date)
        itemView.set(car: ride.car)
    }
}
