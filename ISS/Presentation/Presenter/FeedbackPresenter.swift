//
//  FeedbackPresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class FeedbackPresenter {
    private weak var view: FeedbackView?
    
    init(view: FeedbackView) {
        self.view = view
    }
    
    func sendFeedback() {
        view?.sendMail(with: "lobont.andrei1@gmail.com", and: "CityGO - Feedback")
    }
}
