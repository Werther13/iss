//
//  MainPresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 12/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class MainPresenter {
    private weak var navigation: MainNavigation?
    private let authenticateService: AuthenticationServiceProtocol = AuthenticationService()
    
    init(navigation: MainNavigation) {
        self.navigation = navigation
    }
    
    func viewDidAppear() {
        isUserLogged()
    }
    
    private func isUserLogged() {
        authenticateService.isAuthenticated() ? navigation?.navigateToHome() : navigation?.navigateToLogin()
    }
}
