//
//  ReportCarDetailsPresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class ReportCarDetailsPresenter {
    private weak var view: ReportCarDetailsView?
    var carProblem: CarItem?
    
    init(view: ReportCarDetailsView) {
        self.view = view
    }
    
    func reportBug() {
        if let carProblem = carProblem {
            view?.sendMail(with: "lobont.andrei1@gmail.com", and: "CityGO - Report A Bug - " + carProblem.title)
        }
    }
}
