//
//  MapPresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 15/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class MapPresenter {
    private weak var view: MapView?
    private let fetchCarsService: FetchCarsProtocol = FetchCars()
    private var cars: [Car] = []
    private var carAnnotations: [CarAnnotation] = []
    
    init(view: MapView) {
        self.view = view
    }
    
    func viewDidLoad() {
        fetchCars()
        AppStorage.isFreshInstall = false
    }
    
    func fetchCars() {
        fetchCarsService.fetchCars { (result: Result<[Car], RequestError>) in
            switch result {
            case .success(let cars):
                self.cars = cars
                
                for car in cars {
                    let carAnnotation: CarAnnotation = CarAnnotation(car: car)
                    self.carAnnotations.append(carAnnotation)
                }
                
                self.view?.addAnnotations(with: self.carAnnotations)
                
            case.failure(let error):
                print(error)
            }
        }
    }
}
