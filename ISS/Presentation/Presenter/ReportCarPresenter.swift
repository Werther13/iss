//
//  ReportCarPresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class ReportCarPresenter {
    private weak var view: ReportCarView?
    private var lastItemView: ReportCarCellView?
    private var selectedIndex: Int = 0
    private let carItems: [CarItem] = [.engine, .lights, .notOpening, .windshield, .other]
    
    init(view: ReportCarView) {
        self.view = view
    }
    
    func continuePressed() {
        view?.navigateToBugDetails(with: carItems[selectedIndex])
    }
    
    func numberOfRows() -> Int {
        return carItems.count
    }
    
    func configure(_ itemView: ReportCarCellView, at index: Int) {
        let car = carItems[index]
        itemView.set(content: car.title)
    }
    
    func didSelect(_ itemView: ReportCarCellView, at index: Int) {
        view?.enableContinue()
        if lastItemView != nil {
            lastItemView?.setDeselected()
        }
        itemView.setSelected()
        lastItemView = itemView
    }
}
