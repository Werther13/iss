//
//  ReportBugDetailsPresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class ReportBugDetailsPresenter {
    private weak var view: ReportBugDetailsView?
    var bugType: BugItem?
    
    init(view: ReportBugDetailsView) {
        self.view = view
    }
    
    func reportBug() {
        if let bugType = bugType {
            view?.sendMail(with: "lobont.andrei1@gmail.com", and: "CityGO - Report A Bug - " + bugType.title)
        }
    }
}
