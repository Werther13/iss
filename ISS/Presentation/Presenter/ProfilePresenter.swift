//
//  ProfilePresenter.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

class ProfilePresenter {
    private weak var view: ProfileView?
    private let authenticateService: AuthenticationServiceProtocol = AuthenticationService()
    
    init(view: ProfileView) {
        self.view = view
    }
    
    func viewDidLoad() {
        configure()
    }
    
    func deleteHistory() {
        view?.reloadData()
    }
    
    private func configure() {
        view?.set(email: authenticateService.user?.mail ?? "")
        view?.set(name: authenticateService.user?.name ?? "")
        view?.set(joinDate: "22.10.2020")
        view?.set(numberOfRentCars: "\(AppStorage.ridesHistory.count)")
    }
}
