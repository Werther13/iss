//
//  MapView.swift
//  ISS
//
//  Created by Lobont Andrei on 15/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

protocol MapView: class {
    func addAnnotations(with annotations: [CarAnnotation])
}
