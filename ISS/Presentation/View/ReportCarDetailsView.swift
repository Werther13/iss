//
//  ReportCarDetailsView.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

protocol ReportCarDetailsView: class {
    func sendMail(with email: String, and subject: String)
}
