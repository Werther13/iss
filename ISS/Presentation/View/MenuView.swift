//
//  MenuView.swift
//  ISS
//
//  Created by Andrei Lobont on 11/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

protocol MenuView: class {
    func navigateToFeedback()
    func navigateToVouchers()
    func navigateToAbout()
    func navigateToTerms()
    func navigateToReportBug()
    func navigateToReportCar()
    func signOut()
}
