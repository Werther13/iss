//
//  ProfileView.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

protocol ProfileView: class {
    func set(name: String)
    func set(email: String)
    func set(joinDate: String)
    func set(numberOfRentCars: String)
    func reloadData()
}
