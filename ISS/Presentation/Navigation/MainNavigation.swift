//
//  MainNavigation.swift
//  ISS
//
//  Created by Lobont Andrei on 12/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

protocol MainNavigation: class {
    func navigateToLogin()
    func navigateToHome()
}
