//
//  MapAnnotation.swift
//  ISS
//
//  Created by Lobont Andrei on 22/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

protocol MapAnnotationDelegate {
    func didStartTrip()
}

class MapAnnotation: UIView {
    var delegate: MapAnnotationDelegate?
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var numberOfSeatsLabel: UILabel!
    @IBOutlet weak var carNumberLabel: UILabel!
    @IBOutlet weak var startTripButton: UIButton!
    
    @IBAction func startTrip(_ sender: Any) {
        delegate?.didStartTrip()
    }
    
    class func instantiateNib() -> MapAnnotation {
        guard let view = Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: [:])?.first as? MapAnnotation else { return MapAnnotation() }
        view.bringSubviewToFront(view.carImageView)
        
        return view
    }
    
    func configure(with car: Car) {
        carModelLabel.text = car.manufacturer + " " + car.model
        numberOfSeatsLabel.text = "\(car.numberOfSeats) seats"
        priceLabel.text = "\(car.price) lei / km"
        let carImage = CarImage(rawValue: car.manufacturer.lowercased())
        carImageView.image = UIImage(named: carImage?.name ?? "noImage")
    }
}
