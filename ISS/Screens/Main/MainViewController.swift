//
//  MainViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 12/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    lazy var presenter: MainPresenter = {
        return MainPresenter(navigation: self)
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        presenter.viewDidAppear()
    }
}

extension MainViewController: MainNavigation {
    func navigateToLogin() {
        guard let viewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "Login") as? LoginViewController else { return }
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
    func navigateToHome() {
        guard let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "Home") as? HomeViewController else { return }
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
}
