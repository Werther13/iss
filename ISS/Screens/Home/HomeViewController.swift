//
//  HomeViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 12/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class HomeViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTabBar()
    }
    
    private func setTabBar() {
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.issGreen], for: .selected)
    }
}
