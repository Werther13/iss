//
//  ReportCarViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class ReportCarViewController: UIViewController {
    lazy var presenter: ReportCarPresenter = {
        return ReportCarPresenter(view: self)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var continueButton: GreenButton!
    @IBAction func `continue`(_ sender: Any) {
        presenter.continuePressed()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueButton.alpha = 0.4
        continueButton.isEnabled = false
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.tableFooterView = UIView()
    }
}

extension ReportCarViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ReportCarCell else { return }
        presenter.didSelect(cell, at: indexPath.row)
    }
}

extension ReportCarViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCarCell", for: indexPath) as? ReportCarCell else { return UITableViewCell() }
        
        cell.selectionStyle = .none
        presenter.configure(cell, at: indexPath.row)
        
        return cell
    }
}

extension ReportCarViewController: ReportCarView {
    func navigateToBugDetails(with carProblem: CarItem) {
        guard let viewController = UIStoryboard(name: "ReportCar", bundle: nil).instantiateViewController(identifier: "ReportCarDetailsViewController") as? ReportCarDetailsViewController else { return }
        viewController.presenter.carProblem = carProblem
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func enableContinue() {
        continueButton.alpha = 1
        continueButton.isEnabled = true
    }
}
