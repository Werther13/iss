//
//  MapViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 12/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

private struct Constants {
    static let clujLocation = CLLocation(latitude: 46.76667, longitude: 23.6)
    static let regionRadius: CLLocationDistance = 7000
    static let polylineWidth: CGFloat = 5
}

class MapViewController: UIViewController {
    lazy var presenter: MapPresenter = {
        return MapPresenter(view: self)
    }()
    private let locationManager = CLLocationManager()
    private let infoView: MapAnnotation = MapAnnotation.instantiateNib()
    private var currentCar: Car?
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        infoView.transform = CGAffineTransform(translationX: 0, y: infoView.frame.height)
        checkLocationServiciesAvailability()
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        centerMapOnLocation(location: Constants.clujLocation)
    }
    
    private func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    private func checkLocationServiciesAvailability() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLoactionAuthorization()
        } else {
            
        }
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,latitudinalMeters:
            Constants.regionRadius, longitudinalMeters: Constants.regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    private func checkLoactionAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            break
        case .denied:
            break
        case .authorizedAlways:
            mapView.showsUserLocation = true
            locationManager.startUpdatingLocation()
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            locationManager.startUpdatingLocation()
        @unknown default:
            break
        }
    }
    
    func showCarInfoView(with car: Car) {
        let safeAreaHeightForBottomAnchor = 0 - self.view.safeAreaInsets.bottom
        self.view.addSubview(self.infoView)
        self.infoView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([self.infoView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: safeAreaHeightForBottomAnchor),
                                     self.infoView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor), self.infoView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)])
        
        infoView.show()
        infoView.delegate = self
        currentCar = car
        infoView.configure(with: car)
    }
    
    private func reverseGeocode(with latitude: Double, and longitude: Double, completion: @escaping(String) -> ()) {
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            guard error == nil else { return }
            if let placemarks = placemarks {
                guard placemarks.count > 0 else { return }
                let adress = placemarks[0]
                completion(adress.name ?? "")
            }
        })
    }
}


extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? CarAnnotation else { return }
        
        showCarInfoView(with: annotation.car)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        infoView.hide()
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
}

extension MapViewController: MapView {
    func addAnnotations(with annotations: [CarAnnotation]) {
        mapView.addAnnotations(annotations)
    }
}

extension MapViewController: MapAnnotationDelegate {
    func didStartTrip() {
        if let currentCar = currentCar {
            reverseGeocode(with: currentCar.coordinates.x, and: currentCar.coordinates.y, completion: { (location) in
                let today = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy"
                let ride = Ride(adress: location, date: dateFormatter.string(from: today), car: currentCar.manufacturer + " " + currentCar.model)
                
                var rides = AppStorage.ridesHistory
                rides.append(ride)
                AppStorage.ridesHistory = rides
            })
        }
    }
}
