//
//  NoHistoryView.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class NoContentView: UIView {
    lazy var noContentImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "noCar"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        return imageView
    }()

    lazy var noContentLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "No rides history available"
        label.textColor = .black

        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addImageView()
        addLabel()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    private func addImageView() {
        addSubview(noContentImageView)
        noContentImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        noContentImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }

    private func addLabel() {
        addSubview(noContentLabel)
        noContentLabel.topAnchor.constraint(equalTo: noContentImageView.bottomAnchor, constant: 15).isActive = true
        noContentLabel.centerXAnchor.constraint(equalTo: noContentImageView.centerXAnchor).isActive = true
        noContentLabel.widthAnchor.constraint(lessThanOrEqualToConstant: noContentLabel.intrinsicContentSize.width).isActive = true
        noContentLabel.heightAnchor.constraint(equalToConstant: noContentLabel.intrinsicContentSize.height).isActive = true
    }
}
