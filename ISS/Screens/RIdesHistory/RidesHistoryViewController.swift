//
//  RidesHistoryViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class RidesHistoryViewController: UIViewController {
    lazy var presenter: RidesHistoryPresenter = {
        return RidesHistoryPresenter(view: self)
    }()
    private lazy var noContentView: NoContentView = {
        let noContentView = NoContentView(frame: tableView.frame)
        return noContentView
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.tableFooterView = UIView()
        checkForHistory()
    }
    
    func checkForHistory() {
        presenter.numberOfRows() == 0 ? showNoHistory() : showHistory()
    }
    
    private func showHistory() {
        tableView.backgroundView = UIView()
        tableView.reloadData()
    }
    
    private func showNoHistory() {
        tableView.backgroundView = noContentView
        tableView.reloadData()
    }
}

extension RidesHistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RidesHistoryCell", for: indexPath) as? RidesHistoryCell else { return UITableViewCell() }
        
        cell.selectionStyle = .none
        presenter.configure(cell, at: indexPath.row)
        
        return cell
    }
}

extension RidesHistoryViewController: UITableViewDelegate {
    
}

extension RidesHistoryViewController: RidesHistoryView {
    
}
