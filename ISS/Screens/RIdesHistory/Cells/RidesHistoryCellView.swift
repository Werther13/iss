//
//  RidesHistoryCellView.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

protocol RidesHistoryCellView {
    func set(adress: String)
    func set(date: String)
    func set(car: String)
}
