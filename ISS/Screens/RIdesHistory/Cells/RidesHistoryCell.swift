//
//  RidesHistoryCell.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class RidesHistoryCell: UITableViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var carLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundViews()
    }
    
    private func roundViews() {
        cellView.layer.cornerRadius = 12
        cellView.layer.masksToBounds = true
        dotView.layer.cornerRadius = 4
    }
}

extension RidesHistoryCell: RidesHistoryCellView {
    func set(adress: String) {
        adressLabel.text = adress
    }
    
    func set(date: String) {
        dateLabel.text = date
    }
    
    func set(car: String) {
        carLabel.text = car
    }
}
