//
//  ProfileViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 12/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    lazy var presenter: ProfilePresenter = {
        return ProfilePresenter(view: self)
    }()
    private var ridesHistoryViewController: RidesHistoryViewController = {
        guard let viewController = UIStoryboard(name: "RidesHistory", bundle: nil).instantiateInitialViewController() as? RidesHistoryViewController else { return RidesHistoryViewController() }
        
        return viewController
    }()
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var numberOfRentalsLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var stackViewContainer: UIView!
    
    @IBAction func deleteHistory(_ sender: Any) {
        deleteHistory()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        stackViewContainer.layer.cornerRadius = 12
        stackViewContainer.layer.masksToBounds = true
        showRidesHistory()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeRidesHistory()
    }
    
    private func removeRidesHistory () {
        ridesHistoryViewController.removeFromParent()
        ridesHistoryViewController.view.removeFromSuperview()
    }
    
    private func showRidesHistory() {
        guard let viewController = UIStoryboard(name: "RidesHistory", bundle: nil).instantiateInitialViewController() as? RidesHistoryViewController else { return }
        ridesHistoryViewController = viewController
        contentView.addSubview(ridesHistoryViewController.view)
        addChild(ridesHistoryViewController)
        addViewToContent(view: ridesHistoryViewController.view)
        
        contentView.bringSubviewToFront(ridesHistoryViewController.view)
    }
    
    private func addViewToContent(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([view.topAnchor.constraint(equalTo: contentView.topAnchor),
                                     view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                                     view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                                     view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)])
    }
    
    private func deleteHistory() {
        let alertControl = UIAlertController(title: "Delete History?", message: "Do you really wish to delete rides history? This action can't be undone.", preferredStyle: .actionSheet)
        let alert = UIAlertAction(title: "Delete Rides History", style: .destructive) { (_) in
            self.presenter.deleteHistory()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertControl.addAction(alert)
        alertControl.addAction(cancel)
        
        present(alertControl, animated: true)
    }
}

extension ProfileViewController: ProfileView {
    func reloadData() {
        AppStorage.ridesHistory = []
        ridesHistoryViewController.presenter.rides = []
        ridesHistoryViewController.checkForHistory()
    }
    
    func set(name: String) {
        nameLabel.text = name
    }
    
    func set(email: String) {
        emailLabel.text = email
    }
    
    func set(joinDate: String) {
        dateLabel.text = joinDate
    }
    
    func set(numberOfRentCars: String) {
        numberOfRentalsLabel.text = numberOfRentCars
    }
}
