//
//  LoginViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 12/04/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    lazy var presenter: LoginPresenter = {
        return LoginPresenter(view: self, navigation: self)
    }()
    
    @IBOutlet weak var emailTextField: UnderlinedTextField!
    @IBOutlet weak var passwordTextField: UnderlinedTextField!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBAction func login(_ sender: Any) {
        presenter.login(with: emailTextField.text ?? "", and: passwordTextField.text ?? "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardOnScreenTap()
        containerView.layer.cornerRadius = 12
        logoImageView.layer.cornerRadius = 12
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        logoAnimation()
    }
    
    private func logoAnimation() {
        logoImageView.alpha = 0
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseInOut, animations: {
            self.logoImageView.alpha = 1
        }, completion: nil)
    }
}

extension LoginViewController: LoginView {
    func showLoginFailedAlert() {
        let alert = UIAlertController(title: "Login Failed!", message: "Something went wrong when trying to login.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        present(alert, animated: true)
    }
}

extension LoginViewController: LoginNavigation {
    func navigateToHome() {
        guard let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "Home") as? HomeViewController else { return }
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
}
