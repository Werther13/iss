//
//  VouchersViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 20/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class VouchersViewController: UIViewController {
    @IBOutlet weak var voucherImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        setImageView()
    }
    
    private func setImageView() {
        voucherImageView.layer.cornerRadius = voucherImageView.frame.height / 2
        voucherImageView.layer.borderWidth = 1
        voucherImageView.layer.borderColor = UIColor.gray.cgColor
    }
}
