//
//  MenuTableViewCell.swift
//  ISS
//
//  Created by Lobont Andrei on 11/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    @IBOutlet weak var optionImageView: UIImageView!
    @IBOutlet weak var nameView: UILabel!
    
}

extension MenuTableViewCell: MenuTableViewCellView {
    func set(image: String) {
        optionImageView.image = UIImage(named: image)
    }
    
    func set(name: String) {
        nameView.text = name
    }
}
