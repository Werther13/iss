//
//  MenuViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 11/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class MenuViewController: UITableViewController {
    
    lazy var presenter: MenuPresenter = {
        return MenuPresenter(view: self)
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
}

// MARK: - Table View Data Source
extension MenuViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MenuTableViewCell.self), for: indexPath) as? MenuTableViewCell else { return UITableViewCell() }
        presenter.configure(cell, at: indexPath)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.didSelectItem(at: indexPath)
    }
}

extension MenuViewController: MenuView {
    func signOut() {
        guard let viewController = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() as? LoginViewController else { return }
        viewController.modalPresentationStyle = .fullScreen
        
        present(viewController, animated: true)
    }
    
    func navigateToReportBug() {
        guard let viewController = UIStoryboard(name: "ReportBug", bundle: nil).instantiateInitialViewController() as? ReportBugViewController else { return }
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToReportCar() {
        guard let viewController = UIStoryboard(name: "ReportCar", bundle: nil).instantiateInitialViewController() as? ReportCarViewController else { return }
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFeedback() {
        guard let viewController = UIStoryboard(name: "Feedback", bundle: nil).instantiateInitialViewController() as? FeedbackViewController else { return }
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToVouchers() {
        guard let viewController = UIStoryboard(name: "Vouchers", bundle: nil).instantiateInitialViewController() as? VouchersViewController else { return }
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAbout() {
        guard let viewController = UIStoryboard(name: "About", bundle: nil).instantiateInitialViewController() as? AboutViewController else { return }
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToTerms() {
        guard let viewController = UIStoryboard(name: "Terms", bundle: nil).instantiateInitialViewController() as? TermsViewController else { return }
        
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension UIViewController {
    static func fromStoryboard<T: UIViewController>(name: String, identifier: String? = nil) -> T? {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        if let identifier = identifier {
            return storyboard.instantiateViewController(withIdentifier: identifier) as? T
        } else {
            return storyboard.instantiateInitialViewController() as? T
        }
    }
}
