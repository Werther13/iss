//
//  MenuTableViewCellView.swift
//  ISS
//
//  Created by Lobont Andrei on 11/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

protocol MenuTableViewCellView {
    func set(image: String)
    func set(name: String)
}
