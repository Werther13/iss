//
//  ReportBugCell.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class ReportBugCell: UITableViewCell {
    @IBOutlet weak var checkMarkImageView: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setImageBorder()
    }
    
    private func setImageBorder() {
        checkMarkImageView.layer.cornerRadius = 10
        checkMarkImageView.layer.borderWidth = 1
        checkMarkImageView.layer.borderColor = UIColor.gray.cgColor
    }
}

extension ReportBugCell: ReportBugCellView {
    func set(content: String) {
        contentLabel.text = content
    }
    
    func setSelected() {
        checkMarkImageView.backgroundColor = UIColor.issGreen
    }
    
    func setDeselected() {
        checkMarkImageView.backgroundColor = .white
    }
}
