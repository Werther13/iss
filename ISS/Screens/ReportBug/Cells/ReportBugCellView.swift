//
//  ReportBugCellView.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import Foundation

protocol ReportBugCellView {
    func set(content: String)
    func setSelected()
    func setDeselected()
}
