//
//  ReportBugDetailsViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit
import MessageUI

class ReportBugDetailsViewController: UIViewController {
    lazy var presenter: ReportBugDetailsPresenter = {
        return ReportBugDetailsPresenter(view: self)
    }()
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var reportBugButton: GreenButton!
    @IBOutlet weak var sendBottomConstraint: NSLayoutConstraint!
    @IBAction func reportBug(_ sender: Any) {
        presenter.reportBug()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = false
        hideKeyboardOnScreenTap()
        keyboardNotification()
        setupTextViewStyle()
    }
    
    private func keyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillAppear(_ notification: Notification) {
        let keyboardFrame: NSValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)!
        let keyboardHeight = keyboardFrame.cgRectValue.height
        
        UIView.animate(withDuration: 1) {
            self.sendBottomConstraint.constant = keyboardHeight + 10
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: 1) {
            self.sendBottomConstraint.constant = 60
            self.view.layoutIfNeeded()
        }
    }
    
    private func setupTextViewStyle() {
        textView.layer.borderColor = UIColor.gray.cgColor
        textView.layer.borderWidth = 1
    }
}

extension ReportBugDetailsViewController: ReportBugDetailsView {
    func sendMail(with email: String, and subject: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody(textView.text, isHTML: true)
            mail.setSubject(subject)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
        
    }
}

extension ReportBugDetailsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
        navigationController?.popToRootViewController(animated: true)
    }
}

