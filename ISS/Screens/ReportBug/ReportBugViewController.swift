//
//  ReportBugViewController.swift
//  ISS
//
//  Created by Lobont Andrei on 19/05/2020.
//  Copyright © 2020 Lobont Andrei. All rights reserved.
//

import UIKit

class ReportBugViewController: UIViewController {
    lazy var presenter: ReportBugPresenter = {
        return ReportBugPresenter(view: self)
    }()
    
    @IBOutlet weak var continueButton: GreenButton!
    @IBOutlet weak var tableView: UITableView!

    @IBAction func `continue`(_ sender: Any) {
        presenter.continuePressed()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueButton.alpha = 0.4
        continueButton.isEnabled = false
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.tableFooterView = UIView()
    }
}

extension ReportBugViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ReportBugCell else { return }
        presenter.didSelect(cell, at: indexPath.row)
    }
}

extension ReportBugViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReportBugCell", for: indexPath) as? ReportBugCell else { return UITableViewCell() }
        
        cell.selectionStyle = .none
        presenter.configure(cell, at: indexPath.row)
        
        return cell
    }
}

extension ReportBugViewController: ReportBugView {
    func navigateToBugDetails(with bugType: BugItem) {
        guard let viewController = UIStoryboard(name: "ReportBug", bundle: nil).instantiateViewController(identifier: "ReportBugDetailsViewController") as? ReportBugDetailsViewController else { return }
        viewController.presenter.bugType = bugType
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func enableContinue() {
        continueButton.alpha = 1
        continueButton.isEnabled = true
    }
}
